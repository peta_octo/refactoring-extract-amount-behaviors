# Votre mission, si vous l'acceptez

Deux fonctionnalités dédiées au calcul du montant d'une représentation théâtrale (`performance`) sont disséminées dans la fonction `statement` : le prix total (`totalAmount`) et le prix d'une représentation (`thisAmount`).

Pour améliorer la lisibilité et repérer facilement ces fonctionnalités, on vous demande de les isoler dans des unités de code dont le seul rôle est celui énoncé plus haut : calculer le prix d'une représentation (données d'entrée à déterminer) et le prix total.

N'hésitez pas à relancer régulièrement le test d'acceptance pour éviter les régressions, et à commiter régulièrement vos changements.
